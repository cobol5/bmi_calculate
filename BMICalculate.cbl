       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BMI.
       AUTHOR. JIRAT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 WEIGHT      PIC 999V9 VALUE ZEROS.
       01 HEIGHT      PIC 999V9 VALUE ZEROS.
       01 BMI-RESULT  PIC 99V9  VALUE ZEROS.

       01 BMI-DETAIL  PIC X(35)  VALUE SPACE.
           88 UNDER-WEIGHT       VALUE "Your bmi is Under weight.".
           88 NORMAL-WEIGHT      VALUE "Your bmi is Normal.".
           88 OVER-WEIGHT        VALUE "Your bmi is Over weight.".
           88 OBESE              VALUE "Your bmi is OBESE.".
           88 EXTREMLY-OBESE     VALUE "Your bmi is EXTREMLY OBESE.".

       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY "Input Your Weight(KG) - " WITH NO ADVANCING 
           ACCEPT WEIGHT 
           DISPLAY "Input Your Height(CM) - " WITH NO ADVANCING 
           ACCEPT HEIGHT 

           COMPUTE BMI-RESULT = WEIGHT / ((HEIGHT / 100) ** 2)
           END-COMPUTE 

           EVALUATE TRUE  
              WHEN BMI-RESULT < 18.5 SET UNDER-WEIGHT TO TRUE
              WHEN BMI-RESULT >= 18.5 AND BMI-RESULT < 24.9
              SET NORMAL-WEIGHT TO TRUE
              WHEN BMI-RESULT >= 25.0 AND BMI-RESULT <= 29.9
              SET OVER-WEIGHT TO TRUE
              WHEN BMI-RESULT >= 30.0 AND BMI-RESULT <= 34.9
              SET OBESE TO TRUE
              WHEN BMI-RESULT > 35.0 SET EXTREMLY-OBESE TO TRUE
           END-EVALUATE

           DISPLAY "-------------------------------"
           DISPLAY "Your Weight: "WEIGHT 
           DISPLAY "Your Height: "HEIGHT 
           DISPLAY "BMI Result: "BMI-RESULT 
           DISPLAY "Result: "BMI-DETAIL 
           DISPLAY "-------------------------------"
           GOBACK 
           .
           